package com.example.mytaskapplication.data

data class Departments(val image: String, val students: List<Students>)
