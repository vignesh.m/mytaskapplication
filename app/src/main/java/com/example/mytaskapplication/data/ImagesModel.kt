package com.example.mytaskapplication.data

import java.io.Serializable

data class ImagesModel(val images: ArrayList<String>? = ArrayList()) : Serializable
