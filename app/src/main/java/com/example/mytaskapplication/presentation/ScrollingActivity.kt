package com.example.mytaskapplication.presentation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.example.mytaskapplication.R
import com.example.mytaskapplication.data.Departments
import com.example.mytaskapplication.data.ImagesModel
import com.example.mytaskapplication.databinding.ActivityScrollingBinding
import kotlinx.coroutines.*
import me.relex.circleindicator.CircleIndicator

class ScrollingActivity : AppCompatActivity() {

    private var imagesModel: ImagesModel? = null
    lateinit var viewPagerAdapter: ImageSlideAdapter

    private val adapter = StudentListAdapter()
    private lateinit var binding: ActivityScrollingBinding
    private val viewModel: ScrollingActivityViewModel by viewModels()
    private lateinit var departments: List<Departments>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScrollingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(findViewById(R.id.toolbar))

        binding.recyclerView.layoutManager= LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter

        viewModel.getStudentDetails()

        viewModel.students.observe(this) {
            viewState -> updateUI(viewState)
        }

        binding.etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                adapter.setData(viewModel.onSearchTextChange(s.toString(), departments.get(0).students))
                adapter.notifyDataSetChanged()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        binding.viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                GlobalScope.launch {
                    adapter.setData(departments.get(position).students)
                    delay(200)
                    GlobalScope.launch(Dispatchers.Main) {
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        })

    }

    private fun updateUI(viewState: ScrollingActivityViewState) {
        when(viewState){
            ScrollingActivityViewState.Loading -> {
                binding.progressBar.isVisible = true
                binding.recyclerView.isVisible = false
                binding.errorView.isVisible = false
            }
            is ScrollingActivityViewState.Content -> {
                binding.progressBar.isVisible = false
                binding.recyclerView.isVisible = true
                binding.errorView.isVisible = false

                departments = viewState.departments

                adapter.setData(departments.get(0).students)

                var image: ArrayList<String> = ArrayList()
                for(department in departments)
                    image.add(department.image)

                imagesModel = ImagesModel(image)

                imagesModel?.images?.let{
                    viewPagerAdapter = ImageSlideAdapter(this, it)
                    binding.viewpager.adapter = viewPagerAdapter
                    binding.indicator.setViewPager(binding.viewpager)
                }
            }
            ScrollingActivityViewState.Error -> {
                binding.progressBar.isVisible = false
                binding.recyclerView.isVisible = true
                binding.errorView.isVisible = true
            }
        }
    }

}

