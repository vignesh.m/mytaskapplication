package com.example.mytaskapplication.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mytaskapplication.data.Students
import com.example.mytaskapplication.repository.Repository
import kotlinx.coroutines.launch

class ScrollingActivityViewModel: ViewModel() {
    
    private var _students = MutableLiveData<ScrollingActivityViewState>()
    val students : LiveData<ScrollingActivityViewState>
        get() = _students

    private val repository = Repository()

    fun getStudentDetails(){
        viewModelScope.launch {
            _students.postValue(ScrollingActivityViewState.Loading)
            _students.postValue(ScrollingActivityViewState.Content(repository.getStudentDetail()))
        }
    }

    fun onSearchTextChange(input: String, student: List<Students>): List<Students> {
        return student.filter {
            it.name.lowercase().contains(input.lowercase())
        }
    }
    
}