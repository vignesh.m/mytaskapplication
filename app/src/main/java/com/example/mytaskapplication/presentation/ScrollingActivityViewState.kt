package com.example.mytaskapplication.presentation

import com.example.mytaskapplication.data.Departments

sealed class ScrollingActivityViewState {
    object Loading: ScrollingActivityViewState()
    data class Content(val departments: List<Departments>) : ScrollingActivityViewState()
    object Error: ScrollingActivityViewState()
}
