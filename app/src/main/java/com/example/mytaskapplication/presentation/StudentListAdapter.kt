package com.example.mytaskapplication.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mytaskapplication.R
import com.example.mytaskapplication.data.Students
import com.example.mytaskapplication.databinding.ProductCardBinding

class StudentListAdapter: RecyclerView.Adapter<StudentListAdapter.ViewHolder>() {

    private var studentData: List<Students> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.product_card, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(studentData[position])
    }

    override fun getItemCount(): Int {
        return studentData.size
    }

    fun setData(studentData: List<Students>){
        this.studentData = studentData
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(productCareViewState: Students) {
            val bind = ProductCardBinding.bind(itemView)
            println("name:::"+ productCareViewState.name)
            bind.tvStudentName.text = productCareViewState.name
            bind.tvStudentClass.text = productCareViewState.dep
        }
    }
}