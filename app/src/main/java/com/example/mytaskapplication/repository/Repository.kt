package com.example.mytaskapplication.repository

import com.example.mytaskapplication.data.Departments
import com.example.mytaskapplication.data.Students
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class Repository {
    suspend fun getStudentDetail(): List<Departments> {
        return withContext(Dispatchers.IO){
            delay(1000) // to simulate loading while data fetched from the API

            var image = listOf("https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F3.jpg?alt=media&token=424077d2-6523-466a-aeb5-a775dac914e7",
                "https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F2.jpg?alt=media&token=546939e8-c7be-44ef-84b0-4483ebaac0d3",
                "https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F1.jpg?alt=media&token=ed88baae-a584-4234-a942-63cd540e6c74",
                "https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F5.jpg?alt=media&token=a66ccad1-654a-4fe8-a5df-d11c212d24b5",
                "https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F6.jpg?alt=media&token=45fb1634-465e-4ae2-8691-72f0c3d845bb",
                "https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F8.jpg?alt=media&token=57a8134d-bb26-4be5-b759-a9a536f485ff",
                "https://firebasestorage.googleapis.com/v0/b/cleverpredictions.appspot.com/o/departments%2F7.jpg?alt=media&token=f73c7739-aa45-4d95-a5ef-8cc25d11305f"
            )

            var dep = listOf("CIVIL", "AREO", "CHE", "CSE", "EEE", "IT", "MECK")

            var names = listOf("Liam","Olivia","Noah","Emma","Oliver","Charlotte","Elijah","Amelia","James","Ava","William","Sophia","Benjamin","Isabella","Lucas","Mia","Henry","Evelyn","tTheodore","Harper","Sophie","Hailey","Sadie","Natalia","Quinn","Caroline","Allison","Gabriella","Anna","Serenity","Nevaeh","Cora","Ariana","Emery","Lydia","Jade","Sarah","Eva", "Eliza","Hadley","Melody","Julia","Parker","Rose","Isabelle","Brielle","Adalynn","Arya","Eden","Remi","Mackenzie","Maeve","Margaret","Reagan","Charlie","Alaia","Melanie","Josie","Elliana","Cecilia","Mary","Daisy","Alina","Lucia","Ximena","Juniper","Kaylee","Magnolia","Summer","Adalyn","Sloane","Amara","Arianna","Isabel","Reese","Emersyn","Sienna","Kehlani","River","Freya","Valerie","Blakely","Genevieve","Esther","Valeria","Katherine","Kylie","Norah","Amaya","Bailey","Ember","Ryleigh","Georgia","Catalina","Emerson","Alexandra","Faith","Jasmine","Ariella","Ashley","Andrea","Millie","June","Khloe","Callie","Juliette","Sage","Ada","Anastasia")

            (0..6).map{
                val depId = it
                Departments(image[it],
                        (1..30).map{
                            Students(names.random(), dep[depId])
                        }
                )
            }
        }
    }
}